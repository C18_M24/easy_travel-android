package uk.ac.tees.w9493268.easytravel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashScreen extends AppCompatActivity {

    private static int SPLASHSCREEN= 3000;
    //Variables
    Animation ani_top, ani_bottom;
    ImageView img;
    TextView txtlogo, txtslogan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        ani_top = AnimationUtils.loadAnimation(this, R.anim.ani_top);
        ani_bottom = AnimationUtils.loadAnimation(this, R.anim.ani_bottom);

        img = findViewById(R.id.imgsplash);
        txtlogo = findViewById(R.id.txtlogo);
        txtslogan = findViewById(R.id.txtslogan);

        img.setAnimation(ani_top);
        txtlogo.setAnimation(ani_bottom);
        txtslogan.setAnimation(ani_bottom);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(SplashScreen.this,Register.class);
                startActivity(intent);
                finish();
            }
        },SPLASHSCREEN);
    }
}