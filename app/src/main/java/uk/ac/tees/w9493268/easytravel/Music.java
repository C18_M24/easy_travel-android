package uk.ac.tees.w9493268.easytravel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Music extends AppCompatActivity {
    RecyclerView recyclerView;
    List<Songs> songs = new ArrayList<>();
    Adapter adapter;
    private Adapter.RecyclerViewClickListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);

        recyclerView = findViewById(R.id.songsList);
        songs = new ArrayList<>();
        getjson();
    }

    private void getjson() {

        try {
            String json = readJSONDataFromFile();
            JSONArray jsonArray = new JSONArray(json);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                if (obj != null) {
                    Songs song = new Songs();
                    song.setTitle(obj.getString("title").toString());
                    song.setArtists(obj.getString("artist".toString()));
                    //song.setCoverImage(obj.getString("img_url"));
                    song.setSongURL(obj.getString("web_url"));
                    songs.add(song);
                }
            }
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            setOnClickListner();
            adapter = new Adapter(getApplicationContext(), songs,listener);
            recyclerView.setAdapter(adapter);
            Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

        }
    }

    private void setOnClickListner() {
        listener = new Adapter.RecyclerViewClickListener() {
            @Override
            public void onClick(View v, int position) {
                Intent intent = new Intent(getApplicationContext(),MusicPlayer.class);
                intent.putExtra("url",songs.get(position).getSongURL());
                intent.putExtra("name",songs.get(position).getTitle());
                intent.putExtra("Artist",songs.get(position).getArtists());
                startActivity(intent);
            }
        };
    }

    private String readJSONDataFromFile() throws IOException {
        InputStream inputStream = getAssets().open("songs_json_list.json");
        StringBuilder builder = new StringBuilder();
        try {
            String jsonString = null;
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, "UTF-8"));

            while ((jsonString = bufferedReader.readLine()) != null) {
                builder.append(jsonString);
            }

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return new String(builder);
    }


}