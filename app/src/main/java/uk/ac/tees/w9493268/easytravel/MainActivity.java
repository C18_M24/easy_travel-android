package uk.ac.tees.w9493268.easytravel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;


import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    Toolbar tool_bar;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    ImageView imageView;
    Drawable drawable;
    CardView card_location, card_weather, card_music;
    private static final String TAG = "ListDataActivity";
    DataBaseHelper mDatabaseHelper;
    FirebaseAuth fAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDatabaseHelper = new DataBaseHelper(this);
        fAuth = FirebaseAuth.getInstance();

        //set the images position.
        imageView = findViewById(R.id.imgLocation);
        drawable = getResources().getDrawable(R.drawable.map1);
        imageView.setImageDrawable(drawable);

        imageView = findViewById(R.id.imgWeather);
        drawable = getResources().getDrawable(R.drawable.global);
        imageView.setImageDrawable(drawable);

        imageView = findViewById(R.id.imgMusic);
        drawable = getResources().getDrawable(R.drawable.discmusic);
        imageView.setImageDrawable(drawable);

        //initialize the cardviews
        card_location = (CardView) findViewById(R.id.location_card);
        card_weather = (CardView) findViewById(R.id.weather_card);
        card_music = (CardView) findViewById(R.id.music_card);

        //Set click listerner to the card views
        card_location.setOnClickListener(this);
        card_weather.setOnClickListener(this);
        card_music.setOnClickListener(this);

        //hooks
        drawerLayout = findViewById(R.id.drawer_layout);
        tool_bar = findViewById(R.id.tool_bar);
        navigationView = findViewById(R.id.nav_view);

        //toolbar
        setSupportActionBar(tool_bar);

        //Navigation Drawer menu

        navigationView.bringToFront();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, tool_bar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.id_User);

        // hide and show login and logout
        //Menu menu=navigationView.getMenu();
        //menu.findItem(R.id.id_Logout).setVisible(false);
        //menu.findItem(R.id.id_Profile).setVisible(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sidemenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.id_setting:
                Toast.makeText(getApplicationContext(), "Settings", Toast.LENGTH_SHORT).show();
                break;
            case R.id.id_search:
                Toast.makeText(getApplicationContext(), "Search", Toast.LENGTH_SHORT).show();
                break;
            case R.id.id_Exit:
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(getApplicationContext(), Login.class));
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
              String sEmail="";
        switch (item.getItemId()) {
            case R.id.id_share:
                Toast.makeText(getApplicationContext(), "Share", Toast.LENGTH_SHORT).show();
                break;
            case R.id.id_about:
                Toast.makeText(getApplicationContext(), "about", Toast.LENGTH_SHORT).show();
                break;
            case R.id.id_Switch:
                FirebaseAuth.getInstance().signOut();
                Intent int_Switch = new Intent(MainActivity.this, Login.class);
                startActivity(int_Switch);
                finish();
                break;
            case R.id.id_Logout:
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(getApplicationContext(), Login.class));
                finish();
                break;
            case R.id.id_User:
                if (fAuth.getCurrentUser() != null) {
                   sEmail= fAuth.getCurrentUser().getEmail();
                }
                ViewData(sEmail);
                Toast.makeText(getApplicationContext(), "User", Toast.LENGTH_SHORT).show();
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void ViewData(String email)
    {
        Cursor data = mDatabaseHelper.showData();//mDatabaseHelper.getItemID(email);

        if (data.getCount() == 0) {
            display("Error", "No Data Found.");
            return;
        }
        StringBuffer buffer = new StringBuffer();
        while (data.moveToNext()) {
            buffer.append("ID: " + data.getString(0) + "\n");
            buffer.append("Name: " + data.getString(1) + "\n");
            buffer.append("Email: " + data.getString(2) + "\n");
            display("All Stored Data:", buffer.toString());
        }
    }

    public void display(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.location_card:
                intent = new Intent(this, MapsActivity.class);
                startActivity(intent);
                break;
            case R.id.weather_card:
                intent = new Intent(this, SampleVolleyDemo.class);
                startActivity(intent);
                break;
            case R.id.music_card:
                intent = new Intent(this, Music.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}
