package uk.ac.tees.w9493268.easytravel;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MusicPlayer extends AppCompatActivity {
    private ImageView imagePlayPause;
    private TextView txtCurrentTime, txtTotalDuration,txtTitle,txtArtist;
    private SeekBar playerSeekBar;
    private MediaPlayer mediaPlayer;
    private Handler handler = new Handler();
    String url = "";
    String songName = "";
    String artist= "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_player);

        imagePlayPause = findViewById(R.id.imagePlayPause);
        txtCurrentTime = findViewById(R.id.txtCurrentTime);
        txtTotalDuration = findViewById(R.id.txtTotalDuration);
        playerSeekBar = findViewById(R.id.playerSeekBar);
        txtTitle = findViewById(R.id.txtTitle);
        txtArtist = findViewById(R.id.txtArtist);
        mediaPlayer = new MediaPlayer();

        playerSeekBar.setMax(100);
        imagePlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer.isPlaying()) {
                    handler.removeCallbacks(updater);
                    mediaPlayer.pause();
                    imagePlayPause.setImageResource(R.drawable.icon_play);
                } else {
                    mediaPlayer.start();
                    imagePlayPause.setImageResource(R.drawable.icon_pause);
                    updateSeekBar();
                }
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            url = extras.getString("url");
            songName = extras.getString("name");
            artist = extras.getString("Artist");
            prepareMediaPlayer(url,songName,artist);
        }
        playerSeekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                SeekBar seekBar = (SeekBar) v;
                int playPosition = (mediaPlayer.getDuration() / 100) * seekBar.getProgress();
                mediaPlayer.seekTo(playPosition);
                txtCurrentTime.setText(milliSecondsToTimer(mediaPlayer.getCurrentPosition()));
                return false;
            }
        });

        mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                playerSeekBar.setSecondaryProgress(percent);
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playerSeekBar.setProgress(0);
                imagePlayPause.setImageResource(R.drawable.icon_play);
                txtCurrentTime.setText(R.string.Zero);
                txtTotalDuration.setText(R.string.Zero);
                mediaPlayer.reset();
                prepareMediaPlayer(url,txtTitle.getText().toString(),txtArtist.getText().toString());
            }
        });
    }

    private void prepareMediaPlayer(String url,String songName,String artist) {
        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare();
            txtTotalDuration.setText(milliSecondsToTimer(mediaPlayer.getDuration()));
            txtTitle.setText(songName);
            txtArtist.setText(artist);
        } catch (Exception exception) {
            Toast.makeText(this, exception.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private Runnable updater = new Runnable() {
        @Override
        public void run() {
            updateSeekBar();
            long currentDuration = mediaPlayer.getCurrentPosition();
            txtCurrentTime.setText(milliSecondsToTimer(currentDuration));
        }
    };

    private void updateSeekBar() {
        if (mediaPlayer.isPlaying()) {
            playerSeekBar.setProgress((int) (((float) mediaPlayer.getCurrentPosition() / mediaPlayer.getDuration()) * 100));
            handler.postDelayed(updater, 1000);
        }
    }

    private String milliSecondsToTimer(long milliSeconds) {
        String timerString = "";
        String secondString;

        int hours = (int) (milliSeconds / (1000 * 60 * 60));
        int minutes = (int) (milliSeconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliSeconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        if (hours > 0) {
            timerString = hours + ":";
        }
        if (seconds < 10) {
            secondString = "0" + seconds;
        } else {
            secondString = "" + seconds;
        }
        timerString = timerString + minutes + ":" + secondString;
        return timerString;
    }
}