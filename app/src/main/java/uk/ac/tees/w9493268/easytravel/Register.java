package uk.ac.tees.w9493268.easytravel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Register extends AppCompatActivity {

    DataBaseHelper mDatabaseHelper;

    EditText mFirstName, mLastName, mEmail, mPassword;
    Button mRegisterBtn;
    TextView mAlreadyLoginBtn;
    FirebaseAuth fAuth;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mFirstName = findViewById(R.id.txtFirstName);
        mLastName = findViewById(R.id.txtLastName);
        mEmail = findViewById(R.id.txtEmail);
        mPassword = findViewById(R.id.txtPassword);
        mRegisterBtn = findViewById(R.id.btnRegister);
        mAlreadyLoginBtn = findViewById(R.id.txtGotoLogin);
        mDatabaseHelper = new DataBaseHelper(this);

        fAuth = FirebaseAuth.getInstance();
        progressBar = findViewById(R.id.progressBar);

        if (fAuth.getCurrentUser() != null) {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }

        mRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstName = mFirstName.getText().toString().trim();
                String email = mEmail.getText().toString().trim();
                String password = mPassword.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    mEmail.setError("Email is required.");
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    mPassword.setError("Password is required.");
                    return;
                }

                if (password.length() < 6) {
                    mPassword.setError("Password must be more then 6 characters.");
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                //register the user in fair base.
                fAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(Register.this, "User Created.", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        } else {
                            Toast.makeText(Register.this, "Error !" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });

                if (email.length() != 0) {
                    AddData(email, firstName);
                } else {
                    Toast.makeText(Register.this, "You must put something in the text field!", Toast.LENGTH_SHORT).show();
                }
            }

            private void AddData(String email, String firstName) {
                boolean insertData = mDatabaseHelper.addData(email, firstName);

                if (insertData) {
                    Toast.makeText(Register.this, "Data Successfully Inserted!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Register.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });


        mAlreadyLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Login.class));
            }
        });
    }
}