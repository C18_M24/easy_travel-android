package uk.ac.tees.w9493268.easytravel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface IGoogleAPIService {
    @GET
    Call<MyPlace> getNearByPlaces(@Url String url);
}
