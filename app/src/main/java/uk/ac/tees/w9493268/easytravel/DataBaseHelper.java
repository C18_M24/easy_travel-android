package uk.ac.tees.w9493268.easytravel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class DataBaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "people.db";
    private static final String TAG = "DatabaseHelper";
    private static final String TABLE_NAME = "peopletable";
    private static final String strid = "ID";
    private static final String strname = "NAME";
    private static final String stremail = "Email";
    private Context context;
    private static SQLiteDatabase db;

    public DataBaseHelper(Context context) {
        super(context, TABLE_NAME, null, 1);
        this.context = context;
         db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            String createTable = "CREATE TABLE " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "Email TEXT,NAME TEXT)";
            db.execSQL(createTable);
        }
        catch (SQLException ex)
        {
            Toast.makeText(context,ex.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(String.format("DROP IF TABLE EXISTS %s", TABLE_NAME));
        onCreate(db);
    }

    public boolean addData(String name,String Email) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(strname, name);
        contentValues.put(stremail, Email);

        Log.d(TAG, "addData: Adding " + name + " to " + TABLE_NAME);
        Log.d(TAG, "addData: Adding " + Email + " to " + TABLE_NAME);

        long result = db.insert(TABLE_NAME, null, contentValues);

        //if date as inserted incorrectly it will return -1
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }
    public Cursor showData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    /**
     * Returns only the ID that matches the name passed in
     * @param name
     * @return
     */
    public Cursor getItemID(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + stremail + " FROM " + TABLE_NAME +
                " WHERE " + stremail + " = '" + name + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

}
