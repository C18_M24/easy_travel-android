package uk.ac.tees.w9493268.easytravel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback , GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    double latitude, longitude;
    private Location mLastLocation;
    private Marker mMarker;
    private LocationRequest mLocationRequest;
    String placeType;

    IGoogleAPIService mService;
    private final static int MY_PERMISSION_CODE = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mService = Common.getGoogleAPIService();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        navigationItemSelection();
    }

    private void navigationItemSelection()
    {
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigator);
        bottomNavigationView.setSelectedItemId(R.id.action_gym);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_gym:
                        nearByplaces("gym");//gym//hospital
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.action_library:
                        nearByplaces("library");//library//supermarket
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.action_museum:
                        nearByplaces("museum");//museum//university
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.action_park:
                        nearByplaces("park");//park//restaurant
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });
    }

    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION))
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_CODE);
            else
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_CODE);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null)
                            buildGoogleApiClient();
                        mMap.setMyLocationEnabled(true);
                    }
                } else
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
            }
            break;
        }
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        navigationItemSelection();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        mLastLocation = location;
        if (mMarker != null)
            mMarker.remove();

        latitude = location.getLatitude();
        longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);

        //BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.icon_location);

        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng)
                .title("I am here");
                //.icon(icon);
        mMarker = mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    private void nearByplaces(String placeType) {
        //mMap.clear();
        String url = getUrl(latitude, longitude, placeType);
        mService.getNearByPlaces(url).enqueue(new Callback<MyPlace>() {
                                                  @Override
                                                  public void onResponse(Call<MyPlace> call, Response<MyPlace> response) {
                                                      if (response.isSuccessful()) {
                                                          for (int i = 0; i < response.body().getResults().length; i++) {
                                                              MarkerOptions markerOptions = new MarkerOptions();
                                                              Results googlePlace = response.body().getResults()[i];
                                                              double lat = Double.parseDouble(googlePlace.getGeometry().getLocation().getLat());
                                                              double lng = Double.parseDouble(googlePlace.getGeometry().getLocation().getLng());
                                                              String placeName = googlePlace.getName();
                                                              String Vicinity = googlePlace.getVicinity();
                                                              LatLng latLng = new LatLng(lat, lng);
                                                              markerOptions.position(latLng);
                                                              markerOptions.title(placeName);
                                                              if (placeType.equals("gym")) {
                                                                  markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                                                              } else if (placeType.equals("library")) {
                                                                  markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                                                              } else if (placeType.equals("museum")) {
                                                                  markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
                                                              } else if (placeType.equals("park")) {
                                                                  markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                                                              } else
                                                                  markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                                                              mMap.addMarker(markerOptions);
                                                              mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                                                              mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
                                                          }
                                                      }
                                                  }

                                                  @Override
                                                  public void onFailure(Call<MyPlace> call, Throwable t) {

                                                  }
                                              }
        );
    }

    private String getUrl(double latitude, double longitude, String placeType) {
        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        //location = 54.57160673355494,%20 - 1.2355355768616425 & radius = 1500 & type = hospitals & keyword = cruise & key = AIzaSyARmB6T3VNyl_jNRPB2l69jKhX1PwclIok
        googlePlaceUrl.append("location=" + latitude + "," + longitude);//54.57160673355494,%20-1.2355355768616425"); + latitude + "," + longitude
        googlePlaceUrl.append("&radius=" + 1500);
        googlePlaceUrl.append("&type=" + placeType);
        googlePlaceUrl.append("&key=" +"AIzaSyARmB6T3VNyl_jNRPB2l69jKhX1PwclIok");// getResources().getString(R.string.browser_key));//AIzaSyARmB6T3VNyl_jNRPB2l69jKhX1PwclIok
        Log.d("getUrl", googlePlaceUrl.toString());
        return googlePlaceUrl.toString();
    }
}